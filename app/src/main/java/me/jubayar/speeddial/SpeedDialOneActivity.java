package me.jubayar.speeddial;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.internal.NavigationMenu;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import io.github.yavski.fabspeeddial.FabSpeedDial;
import io.github.yavski.fabspeeddial.SimpleMenuListenerAdapter;

public class SpeedDialOneActivity extends AppCompatActivity {

    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speed_dial_one);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        final FrameLayout frameLayout = (FrameLayout) findViewById(R.id.lay);
        frameLayout.setVisibility(View.VISIBLE);
        frameLayout.getBackground().setAlpha(0);

        FabSpeedDial fabSpeedDial = (FabSpeedDial) findViewById(R.id.fab_speed_dial);
        fabSpeedDial.setMenuListener(new SimpleMenuListenerAdapter() {
            @Override
            public boolean onPrepareMenu(NavigationMenu navigationMenu) {
                frameLayout.getBackground().setAlpha(200);
                return true;
            }

            @Override
            public void onMenuClosed() {
                super.onMenuClosed();
                frameLayout.getBackground().setAlpha(0);
            }

            @Override
            public boolean onMenuItemSelected(MenuItem menuItem) {

                int id = menuItem.getItemId();

                if (id == R.id.action_call) {
                    Toast.makeText(getApplicationContext(), "Call press", Toast.LENGTH_LONG).show();
                }

                return false;
            }
        });
    }

    public static void start(Context activityContext) {
        activityContext.startActivity(new Intent(activityContext, SpeedDialOneActivity.class));
    }
}
